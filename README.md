[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/roubine/docker/badges/main/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/roubine/docker/-/commits/main)

# My docker images
Used for gitlab CI and/or quick tests.


## Root image
- debian 10 "buster"
- python3 `3.7.3`
- `CMD ["bash"]`

Image address
```
gricad-registry.univ-grenoble-alpes.fr/roubine/docker
```

Run short lived container
```bash
docker run -it gricad-registry.univ-grenoble-alpes.fr/roubine/docker
```

## Python 3.7
- debian 10 "buster"
- python `3.7.12`
- `requirements.txt` installed
- `CMD ["bash"]`

Image address
```
gricad-registry.univ-grenoble-alpes.fr/roubine/docker/python3.7
```

Run short lived container
```bash
docker run -it gricad-registry.univ-grenoble-alpes.fr/roubine/docker/python3.7
```

## Python 3.10
- debian 11 "bullseye"
- python `3.10.*`
- `requirements.txt` installed
- `CMD ["bash"]`

Image address
```
gricad-registry.univ-grenoble-alpes.fr/roubine/docker/python3.10
```

Run short lived container
```bash
docker run -it gricad-registry.univ-grenoble-alpes.fr/roubine/docker/python3.10
```
