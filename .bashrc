# terminal color
NM="\[\033[0;38m\]"; HI="\[\033[0;37m\]"; HII="\[\033[0;36m\]"; SI="\[\033[0;33m\]"; IN="\[\033[0m\]";
export PS1="$NM[ $HI\u $HII\h $SI\w$NM ] $IN"

# aliases
alias e='emacs -nw'
alias l='ls -rtl --color'
alias ls='ls --color'

# export python virtualenv path
# export PATH="/root/.venv/bin:${PATH}"
