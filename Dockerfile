FROM debian:buster

##########
# SYSTEM #
##########
# workdir
WORKDIR /root

# install system dependencies
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get clean && apt-get update && apt-get install -y apt-utils
RUN apt-get install -y \
    build-essential \
    git \
    gfortran \
    rcs \
    libx11-dev \
    python3-dev python3-venv python3-pip \
    emacs-nox

# copy configuration files
COPY .bashrc .
COPY .emacs .

#######
# CMD #
#######
CMD ["bash"]
